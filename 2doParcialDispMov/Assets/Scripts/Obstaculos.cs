﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstaculos : MonoBehaviour
{
    public float speed;
    float tiempo=2;
    int MovY,ran;
    int damage=30;
    private void Start()
    {
        ran = Random.Range(0,2);
    }
    private void Update()
    {
        transform.Translate(new Vector2(-1, MovY) * speed * Time.deltaTime);
        tiempo -= Time.deltaTime;
        if (tiempo <= 0)
        {
            movAleatorio();
        }
    }  
    void movAleatorio()
    {
        ran = Random.Range(0, 2);
        switch (ran)
        {
            case 0:
                MovY = 1;
                tiempo = 2;
                break;
            case 1:
                MovY = -1;
                tiempo = 2;
                break;
        }
    }
    void OnTriggerEnter2D(Collider2D hitInfo)
    {
        PContoler player = hitInfo.GetComponent<PContoler>();
        if (player != null)
        {
            player.TakeDamage(damage);
            //  Instantiate(impactEffect, transform.position, transform.rotation);
            Destroy(gameObject);
        }
    }
}
