﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateAlly : MonoBehaviour
{

    public Transform player;

    float speed = 5f;

    private void Update()
    {
        Vector3 vectorToTarget = player.position - transform.position;
        float angle = Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg;
        Quaternion q = Quaternion.AngleAxis(angle - 90, Vector3.forward);
        transform.rotation = Quaternion.Slerp(transform.rotation, q, Time.deltaTime * speed);
    }

}
