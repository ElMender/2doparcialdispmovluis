﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletEnemy : MonoBehaviour
{
    public float speed = 5f;
    public float bulletLife = 1;
    public int damage = 1;
    Rigidbody2D rb;
   

    // Use this for initialization
    void Start()
    {
       rb =this.GetComponent<Rigidbody2D>();
        rb.velocity =  transform.up * speed;
    }

    void Update()
    {
        
        Destroy(gameObject, bulletLife);
    }
    void OnTriggerEnter2D(Collider2D hitInfo)
    {
        PContoler player = hitInfo.GetComponent<PContoler>();
        if (player != null)
        {
            player.TakeDamage(damage);
            //  Instantiate(impactEffect, transform.position, transform.rotation);
            Destroy(gameObject);

        }
    }
}
