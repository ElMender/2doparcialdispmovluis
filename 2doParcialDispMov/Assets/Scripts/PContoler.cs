﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PContoler : MonoBehaviour
{
    int life = 10;
    public float speed;
    public Transform ShotPoint;
    public GameObject Bullet,PanelGameOver;
    public float firerate = 0.5f;
    public bool PowerUp = false;

    private Gyroscope gyroscope;
    private bool gyroEnabled;

    float actitudxd;

    //private void Start()
    //{
    //    gyroEnabled = EnableGyro();
    //}

    //private bool EnableGyro()
    //{
    //    if (SystemInfo.supportsGyroscope)
    //    {
    //        gyroscope = Input.gyro;
    //        gyroscope.enabled = true;
    //        return true;
    //    }
    //
    //    return false;
    //}
    private void Update()
    {

            actitudxd = Input.acceleration.x;

            if (actitudxd > 0)
            {
                transform.position = new Vector2(transform.position.x + 0.01f, transform.position.y);
            }
            else if (actitudxd < 0)
            {
                transform.position = new Vector2(transform.position.x - 0.01f, transform.position.y);
            }
            else
            {
                transform.position = new Vector2(transform.position.x, transform.position.y);
            }
    }

    public void Shoot(Transform shotPointG)
    {
        Instantiate(Bullet, shotPointG.position, Quaternion.identity);
    }

    public void TakeDamage(int damage)
    {
        //Debug.Log(damage);
        life -= damage;
        if (life <= 0)
        {
            //Debug.Log("muerto");
            PanelGameOver.SetActive(true);
            gameObject.SetActive(false);
        }
    }
}
