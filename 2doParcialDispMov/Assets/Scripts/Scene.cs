﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Scene : MonoBehaviour
{
    public void Menu()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void Lvl1()
    {
        SceneManager.LoadScene("Lvl1");
    }

    public void Boss()
    {
        SceneManager.LoadScene("Boss");
    }

}
