﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    int life = 100;
    public float speed,speedLat;
    float SpawnTime;
    public float firerate = 0.5f;
    public GameObject Bullet;
    public Transform ShotPoint;
    public void TakeDamage(int damage)
    {
           //Debug.Log(damage);
        life -= damage;
        if (life <= 0)
        {
            //Debug.Log("muerto");
            Destroy(gameObject);
        }
    }
    private void Update()
    {
        if (Time.time > SpawnTime)
        {
            SpawnTime = Time.time + firerate;
            Instantiate(Bullet, ShotPoint.position, ShotPoint.rotation);
        }
        transform.Translate(new Vector2(0,-1)*speed*Time.deltaTime);
    }

}
